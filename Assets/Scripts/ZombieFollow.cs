﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.AI;

public class ZombieFollow : MonoBehaviour
{
    public GameObject Player;
    public GameObject ZombieEnemy;
    public double PlayerDistance;
    public double AttackDistance = 10;
    public float ZombieSpeed;
    public bool AttackFlag = false;
    public RaycastHit Shot;
    public bool IsAttacking = false;
    public GameObject ScreenFlash;
    public AudioSource PlayerHurt01;
    public AudioSource PlayerHurt02;
    public AudioSource PlayerHurt03;


    void Update()
    {
        var distance = Vector3.Distance(Player.transform.position, transform.position);
        var nav = GetComponent<NavMeshAgent>();
        if (distance <= 15)
        {
            if (AttackFlag == false)
            {
                nav.enabled = true;
                GetComponent<NavMeshAgent>().destination = Player.transform.position;
                ZombieEnemy.GetComponent<Animation>().Play("Z_Walk_InPlace");
            } 
            else
            {
                nav.enabled = false;
            }
        }
        else
        {
            nav.enabled = false;
            ZombieEnemy.GetComponent<Animation>().Play("Z_Idle");
        }

        if (AttackFlag == true)
        {
            if(IsAttacking == false)
            {
                StartCoroutine(EnemyDamage());
            }
            ZombieSpeed = 0;
            ZombieEnemy.GetComponent<Animation>().Play("Z_Attack");
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        AttackFlag = true;
    }

    private void OnTriggerExit(Collider other)
    {
        AttackFlag = false;
    }
    
    private IEnumerator EnemyDamage()
    {
        IsAttacking = true;
        int PainSound = Random.Range(1, 4);
        yield return new WaitForSeconds(0.2f);
        ScreenFlash.SetActive(true);
        GlobalHealth.PlayerHealth -= 5;
        if(PainSound == 1)
        {
            PlayerHurt01.Play();
        }

        if (PainSound == 2)
        {
            PlayerHurt02.Play();
        }

        if (PainSound == 3)
        {
            PlayerHurt03.Play();
        }
        yield return new WaitForSeconds(0.1f);
        ScreenFlash.SetActive(false);
        yield return new WaitForSeconds(0.2f);
        IsAttacking = false;
    }
}
