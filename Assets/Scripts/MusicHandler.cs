﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicHandler : MonoBehaviour
{
    public bool isMusic = false;
    void Start()
    {
        float volume = 0f;
        if (isMusic)
        {
            volume = PlayerPrefs.GetFloat("Music");
        }
        else
        {
            volume = PlayerPrefs.GetFloat("Sounds");
        }
        gameObject.GetComponent<AudioSource>().volume = volume;
    }

    void Update()
    {
        
    }
}
