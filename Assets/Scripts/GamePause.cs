﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class GamePause : MonoBehaviour
{
    public static bool Paused = false;
    public GameObject Player;
    public GameObject pauseText;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Pause"))
        {
            if (!Paused)
            {
                Time.timeScale = 0;
                Paused = true;
                Player.GetComponent<FirstPersonController>().enabled = false;
                Cursor.visible = true;
                pauseText.SetActive(true);
            }
            else
            {
                Time.timeScale = 1;
                Paused = false;
                Player.GetComponent<FirstPersonController>().enabled = true;
                pauseText.SetActive(false);
            }
        }
    }
}
