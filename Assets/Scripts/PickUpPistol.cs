﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpPistol : MonoBehaviour
{
    public float TheDistance = PlayerCasting.DistanceFromTarget;
    public GameObject TextDisplay;

    public GameObject LaserCarbine;
    public GameObject LaserPistol;
    public GameObject FakeGun;
    public AudioSource PickUpAudio;

    // Update is called once per frame
    void Update()
    {
        TheDistance = PlayerCasting.DistanceFromTarget;
    }

    void OnMouseOver()
    {
        if (TheDistance <= 2)
        {
            TextDisplay.GetComponent<UnityEngine.UI.Text>().text = "Laser Pistol";
        }
        if (Input.GetButtonDown("Action"))
        {
            if (TheDistance <= 2)
            {
                StartCoroutine(TakePistol());
            }
        }
    }

    void OnMouseExit()
    {
        TextDisplay.GetComponent<UnityEngine.UI.Text>().text = "";
    }

    IEnumerator TakePistol()
    {
        PickUpAudio.Play();
        transform.position = new Vector3(0, -1000, 0);
        FakeGun.SetActive(false);
        LaserCarbine.SetActive(false);
        LaserPistol.SetActive(true);
        WeaponChange.hasPistol = true;
        yield return new WaitForSeconds(0.1f);
    }
}
