﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GlobalHealth : MonoBehaviour
{
    public static int PlayerHealth = 100;
    public int InternalHealth;
    public GameObject HealthObject;
    void Update()
    {
        InternalHealth = PlayerHealth;
        HealthObject.GetComponent<Text>().text = "Health : " + PlayerHealth;
        if(PlayerHealth == 0)
        {
            SceneManager.LoadScene(1);
        }
    }
}
