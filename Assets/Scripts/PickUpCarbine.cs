﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using static PlayerCasting;

public class PickUpCarbine : MonoBehaviour
{
    public float TheDistance = PlayerCasting.DistanceFromTarget;
    public GameObject TextDisplay;

    public GameObject LaserCarbine;
    public GameObject LaserPistol;
    public GameObject FakeGun;
    public AudioSource PickUpAudio;

    void Update()
    {
        TheDistance = PlayerCasting.DistanceFromTarget;
    }
    
    void OnMouseOver()
    {
        if (TheDistance <= 2)
        {
            TextDisplay.GetComponent<UnityEngine.UI.Text>().text = "Laser Carbine";
        }
        if (Input.GetButtonDown("Action"))
        {
            if (TheDistance <= 2)
            {
                StartCoroutine(TakeNineMil());
            }
        }
    }

    void OnMouseExit()
    {
        TextDisplay.GetComponent<UnityEngine.UI.Text>().text = "";
    }

    IEnumerator TakeNineMil()
    {
        PickUpAudio.Play();
        transform.position = new Vector3(0, -1000, 0);
        FakeGun.SetActive(false);
        LaserPistol.SetActive(false);
        LaserCarbine.SetActive(true);
        WeaponChange.hasCarbine = true;
        yield return new WaitForSeconds(0.1f);
    }
}


