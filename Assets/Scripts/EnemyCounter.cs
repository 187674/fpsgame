﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyCounter : MonoBehaviour
{
    public static int enemyLeft;
    // Start is called before the first frame update
    void Start()
    {
        enemyLeft = 7;
    }

    // Update is called once per frame
    void Update()
    {
        if(enemyLeft == 0)
        {
            StartCoroutine(Wait());
            SceneManager.LoadScene(5);
        }
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(2);
    }
}
