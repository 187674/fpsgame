﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponChange : MonoBehaviour
{
    public static bool hasPistol = false;
    public static bool hasCarbine = false;

    public GameObject LaserCarbine;
    public GameObject LaserPistol;

    // Update is called once per frame
    void Update()
    {
        if (GamePause.Paused) return;
        if (Input.GetButtonDown("Pistol"))
        {
            if (hasPistol)
            {
                LaserCarbine.SetActive(false);
                LaserPistol.SetActive(true);
            }
        }
        if (Input.GetButtonDown("Carbine"))
        {
            if (hasCarbine)
            {
                LaserPistol.SetActive(false);
                LaserCarbine.SetActive(true);
            }
        }
    }
}
