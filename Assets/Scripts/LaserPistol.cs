﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserPistol : MonoBehaviour
{
    AudioSource audioData;
    Animation anim;
    public GameObject Flash;

    public IEnumerator MuzzleOff()
    {

        yield return new WaitForSeconds(0.15f);
        Flash.SetActive(false);
    }

    // Start is called before the first frame update
    void Start()
    {
        audioData = GetComponent<AudioSource>();
        anim = GetComponent<Animation>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GamePause.Paused) return;
        if (Input.GetButtonDown("Fire1"))
        {
            if (GlobalAmmo.CurrentAmmo == 0) return;
            audioData.Play();
            Flash.SetActive(true);
            StartCoroutine(MuzzleOff());
            anim.Play("pistol_shot");
            GlobalAmmo.CurrentAmmo--;
        }
    }
}
