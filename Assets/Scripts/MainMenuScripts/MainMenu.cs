﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void PlayGame()
    {
        GameObject.FindGameObjectWithTag("Music").GetComponent<AudioSource>().Stop();
        SceneManager.LoadScene(2);
    }

    public void ShowAuthors()
    {
        SceneManager.LoadScene(3);
    }

    public void OpenOptions()
    {
        SceneManager.LoadScene(4);
    }
}
