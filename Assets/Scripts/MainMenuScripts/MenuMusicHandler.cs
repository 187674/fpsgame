﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuMusicHandler : MonoBehaviour
{

    public void Awake()
    {
        GameObject[] objects = GameObject.FindGameObjectsWithTag("Music");
        if (objects.Length > 1)
        {
            Destroy(this.gameObject);
        }
        else
        {
            this.gameObject.GetComponent<AudioSource>().volume = PlayerPrefs.GetFloat("Music");
            DontDestroyOnLoad(transform.gameObject);
        }
    }
}
