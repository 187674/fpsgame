﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AuthorssToMenu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(ReturnToMenu());
    }

    private IEnumerator ReturnToMenu()
    {
        yield return new WaitForSeconds(10);
        SceneManager.LoadScene(0);
    }
}
