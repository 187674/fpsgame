﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GlobalOptions : MonoBehaviour
{
    public Slider Music;
    public Slider Sounds;

    // Start is called before the first frame update
    public void Start()
    {
        Music.value = PlayerPrefs.GetFloat("Music");
        Sounds.value = PlayerPrefs.GetFloat("Sounds");
        GameObject.FindGameObjectWithTag("Music").GetComponent<AudioSource>().volume = Music.value;
    }

    // Update is called once per frame
    public void Update()
    {
        GameObject.FindGameObjectWithTag("Music").GetComponent<AudioSource>().volume = Music.value;
    }

    public void SaveOptions()
    {
        PlayerPrefs.SetFloat("Music", Music.value);
        PlayerPrefs.SetFloat("Sounds", Sounds.value);
        SceneManager.LoadScene(0);
    }
}
