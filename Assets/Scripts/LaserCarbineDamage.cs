﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserCarbineDamage : MonoBehaviour
{
    public int PistolDamageAmount = 3;
    public int CarbineDamageAmount = 5;
    public float TargetDistance;
    public float PistolAllowedRange = 15.0f;
    public float CarbineAllowedRange = 25.0f;
    public GameObject LaserCarbine;
    public GameObject LaserPistol;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            if (GamePause.Paused) return;
            if (GlobalAmmo.CurrentAmmo == 0) return;

            RaycastHit Shot;

            if (LaserCarbine.activeSelf)
            {
                if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out Shot))
                {
                    TargetDistance = Shot.distance;
                    if (TargetDistance < CarbineAllowedRange)
                    {
                        Shot.transform.SendMessage("DeductPoints", CarbineDamageAmount, SendMessageOptions.DontRequireReceiver);
                    }
                }
            }

            if (LaserPistol.activeSelf)
            {
                if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out Shot))
                {
                    TargetDistance = Shot.distance;
                    if (TargetDistance < PistolAllowedRange)
                    {
                        Shot.transform.SendMessage("DeductPoints", PistolDamageAmount, SendMessageOptions.DontRequireReceiver);
                    }
                }
            }


        }
    }
}
